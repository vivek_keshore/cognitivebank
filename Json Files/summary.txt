url - /banking/summary/
Request - GET

HTTP HEADER
bank-auth-token: <accesstoken>

Output Json:

{
    "balance": 17278,
    "customer_id": 2,
    "account_type": "SAVINGS",
    "account_id": 2,
    "transactions": [
        {
            "transaction_type": "DEPOSIT",
            "timestamp": "2016-08-21T18:11:44.865211+05:30",
            "merchant_id": 2,
            "transaction_amount": 7449,
            "merchant_name": "Customer_1"
        },
        {
            "transaction_type": "WITHDRAWAL",
            "timestamp": "2016-08-21T18:11:44.876696+05:30",
            "merchant_id": 2,
            "transaction_amount": 1933,
            "merchant_name": "Customer_1"
        },
        {
            "transaction_type": "DEPOSIT",
            "timestamp": "2016-08-21T18:11:44.900305+05:30",
            "merchant_id": 2,
            "transaction_amount": 5245,
            "merchant_name": "Customer_1"
        },
        {
            "transaction_type": "WITHDRAWAL",
            "timestamp": "2016-08-21T18:11:44.920975+05:30",
            "merchant_id": 2,
            "transaction_amount": 2643,
            "merchant_name": "Customer_1"
        },
        {
            "transaction_type": "DEPOSIT",
            "timestamp": "2016-08-21T18:11:44.944386+05:30",
            "merchant_id": 2,
            "transaction_amount": 9660,
            "merchant_name": "Customer_1"
        }
    ]
}
