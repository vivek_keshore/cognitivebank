CongnitiveBank README

Routes and services
------------------------------------------------------------------------------------------------
Name                Pattern                      View                                     Method
------------------------------------------------------------------------------------------------
login               /auth/login/                 apps.auth.views.AuthLoginView            POST
logout              /auth/logout/                apps.auth.views.AuthLogoutView           DELETE
customer_details    /auth/customer/details/      apps.auth.views.CustomerDetailsView      GET
balance             /banking/balance/            apps.banking.views.GetBalanceView        GET
summary             /banking/summary/            apps.banking.views.GetSummaryView        GET
add_beneficiary     /banking/add/beneficiary/    apps.banking.views.AddBeneficiaryView    POST
fund_transfer       /banking/transfer/funds/     apps.banking.views.FundTransferView      POST


Setting up project:
1. Install postgres database.
2. Setup virtualenv
3. Install pip packages
 pip install -r requirements.txt

4. Setup application
    python setup.py develop

5. Load sample data
    python dataloader.py

6. Start Server
    pserve development.ini


Assumptions:
Login response will provide an 'access_token' which should be added to http header to make authorized requests.
  name: bank-auth-token
  value: <access_token>

Sample json are provided in separate files.

Sample usernames        password
---------------------------------
customer_0@bank.com     12345678
customer_1@bank.com     12345678
customer_2@bank.com     12345678
customer_3@bank.com     12345678
customer_4@bank.com     12345678
customer_5@bank.com     12345678
customer_6@bank.com     12345678
customer_7@bank.com     12345678
customer_8@bank.com     12345678
customer_9@bank.com     12345678
