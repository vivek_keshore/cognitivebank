import httplib
from functools import wraps
from jsonschema import validate, ValidationError
from jsonschema import draft4_format_checker
from service.auth import AuthService
from settings import HTTP_HEADER_AUTH_TOKEN


class AppRoute(object):
    def __init__(self, name, path, verb='GET'):
        self.name = name
        self.path = path
        self.verb = verb


class ViewBase(object):
    def __init__(self, request):
        if not request:
            return
        self.request = request

    @classmethod
    def includeme(cls, config):
        routes = getattr(cls, 'routes', None)
        if not callable(routes):
            return

        for route in routes():
            config.add_route(route.name, route.path, request_method=route.verb)


def validate_json_input(schema):
    """
    Decorator to validate the request's JSON input

    Return ``httplib.BAD_REQUEST`` on error.

    It will ensure:
    - JSON input (request.body) validates against the given JSON ``schema``

    """
    def decorator(view_method):
        @wraps(view_method)
        def wrapper(self):
            error = None
            try:
                validate(self.request.json_body, schema,
                         format_checker=draft4_format_checker)
            except ValueError as err:
                error = str(err)
            except ValidationError as err:
                error = 'Invalid JSON: {}'.format(err.message)

            if error is not None:
                return {httplib.BAD_REQUEST: error}

            return view_method(self)
        return wrapper
    return decorator


def authorize():
   def decorator(view_method):
       @wraps(view_method)
       def wrapper(self):
           access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
           if not access_token:
               return {httplib.UNAUTHORIZED: 'Invalid Access Token'}
           service = AuthService(self.request)
           response = service.validate_token(access_token)
           if response == httplib.NO_CONTENT:
               return {httplib.UNAUTHORIZED: 'User is not authenticated.'}
           elif response == httplib.FORBIDDEN:
               return {httplib.UNAUTHORIZED: 'Access token expired.'}
           return view_method(self)
       return wrapper
   return decorator