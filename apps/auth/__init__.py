from apps import ViewBase, AppRoute


def includeme(config):
    AuthViewBase.includeme(config)


class AuthViewBase(ViewBase):
    """ Views related to Auth """

    @staticmethod
    def routes():
        return [
            AppRoute('login', '/login/', 'POST'),
            AppRoute('logout', '/logout/', 'DELETE'),
            AppRoute('customer_details', '/customer/details/', 'GET'),
        ]
