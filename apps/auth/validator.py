PROP_USERNAME = 'username'
PROP_PASSWORD = 'password'

SCHEMA_DEF = {
    PROP_USERNAME: {'type': 'string', 'format': 'email'},
    PROP_PASSWORD: {'type': 'string', 'minLength': 8},
}

USER_LOGIN_SCHEMA = {
    'title': 'User Login',
    'type': 'object',
    'required': [PROP_USERNAME, PROP_PASSWORD],
    'additionalProperties': False,
    'properties': {
        PROP_USERNAME: SCHEMA_DEF[PROP_USERNAME],
        PROP_PASSWORD: SCHEMA_DEF[PROP_PASSWORD],
    }
}
