from pyramid.view import view_config
from apps import validate_json_input, authorize
from apps.auth import AuthViewBase
from apps.auth.validator import USER_LOGIN_SCHEMA
from service.auth import AuthService
from utils import strip_spaces_in_string
from settings import HTTP_HEADER_AUTH_TOKEN


@view_config(route_name='login', renderer='json')
class AuthLoginView(AuthViewBase):
    """
        This view can be used to validate user credentials
        when a user trying to login to bank.
    """

    @validate_json_input(USER_LOGIN_SCHEMA)
    def __call__(self):
        auth_json = self.request.json_body
        username = strip_spaces_in_string(auth_json.get('username'))
        password = strip_spaces_in_string(auth_json.get('password'))

        service = AuthService(self.request)
        return service.login(username, password)


@view_config(route_name='logout', renderer='json')
class AuthLogoutView(AuthViewBase):
    """
        This view can be used to remove access tokens and logout.
    """

    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        service = AuthService(self.request)
        return service.logout(access_token)

@view_config(route_name='customer_details', renderer='json')
class CustomerDetailsView(AuthViewBase):
    """
        This view can be used to show details of a customer.
    """

    @authorize()
    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        service = AuthService(self.request)
        return service.customer_details(access_token)
