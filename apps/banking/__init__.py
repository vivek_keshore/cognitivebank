from apps import ViewBase, AppRoute


def includeme(config):
    BankingViewBase.includeme(config)


class BankingViewBase(ViewBase):
    """ Views related to Auth """

    @staticmethod
    def routes():
        return [
            AppRoute('balance', '/balance/', 'GET'),
            AppRoute('summary', '/summary/', 'GET'),
            AppRoute('add_beneficiary', '/add/beneficiary/', 'POST'),
            AppRoute('fund_transfer', '/transfer/funds/', 'POST'),
        ]
