PROP_BRANCH_ID = 'branch_id'
PROP_LIMIT = 'amount_limit'
PROP_EMAIL = 'email'
PROP_ACCOUNT_ID = 'account_id'
PROP_BENEFICIARTY_ID = 'beneficiary_id'
PROP_AMOUNT = 'amount'

SCHEMA_DEF = {
    PROP_EMAIL: {'type': 'string', 'format': 'email'},
    PROP_BRANCH_ID: {'type': 'integer', 'minLength': 1},
    PROP_LIMIT: {'type': 'integer', 'minLength': 1},
    PROP_ACCOUNT_ID: {'type': 'integer', 'minLength': 1},
    PROP_BENEFICIARTY_ID: {'type': 'integer', 'minLength': 1},
    PROP_AMOUNT: {'type': 'integer', 'minLength': 1},
}

ADD_BENEFICIARY_SCHEMA = {
    'title': 'Add beneficiary',
    'type': 'object',
    'required': [PROP_ACCOUNT_ID, PROP_BRANCH_ID, PROP_EMAIL, PROP_LIMIT],
    'additionalProperties': False,
    'properties': {
        PROP_EMAIL: SCHEMA_DEF[PROP_EMAIL],
        PROP_ACCOUNT_ID: SCHEMA_DEF[PROP_ACCOUNT_ID],
        PROP_BRANCH_ID: SCHEMA_DEF[PROP_BRANCH_ID],
        PROP_LIMIT: SCHEMA_DEF[PROP_LIMIT],
    }
}

FUND_TRANSFER_SCHEMA = {
    'title': 'Funds Transfer',
    'type': 'object',
    'required': [PROP_BENEFICIARTY_ID, PROP_AMOUNT],
    'additionalProperties': False,
    'properties': {
        PROP_BENEFICIARTY_ID: SCHEMA_DEF[PROP_BENEFICIARTY_ID],
        PROP_AMOUNT: SCHEMA_DEF[PROP_AMOUNT]
    }
}