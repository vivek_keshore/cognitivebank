from pyramid.view import view_config
from apps import authorize
from apps import validate_json_input
from apps.banking import BankingViewBase
from apps.banking.validator import ADD_BENEFICIARY_SCHEMA
from apps.banking.validator import FUND_TRANSFER_SCHEMA
from service.banking import BankingService
from settings import HTTP_HEADER_AUTH_TOKEN


@view_config(route_name='balance', renderer='json')
class GetBalanceView(BankingViewBase):
    """
        This view is used to get the account balance.
    """

    @authorize()
    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        service = BankingService(self.request)
        return service.get_balance(access_token)


@view_config(route_name='summary', renderer='json')
class GetSummaryView(BankingViewBase):
    """
        This view is used to get the account summary.
    """

    @authorize()
    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        service = BankingService(self.request)
        return service.get_summary(access_token)


@view_config(route_name='add_beneficiary', renderer='json')
class AddBeneficiaryView(BankingViewBase):
    """
        This view is used to add beneficiary to account.
    """

    @validate_json_input(ADD_BENEFICIARY_SCHEMA)
    @authorize()
    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        ben_json = self.request.json_body
        service = BankingService(self.request)
        return service.add_beneficiary(access_token, ben_json)


@view_config(route_name='fund_transfer', renderer='json')
class FundTransferView(BankingViewBase):
    """
        This view is used to transfer funds to beneficiary account.
    """

    @validate_json_input(FUND_TRANSFER_SCHEMA)
    @authorize()
    def __call__(self):
        access_token = self.request.headers.get(HTTP_HEADER_AUTH_TOKEN)
        fund_json = self.request.json_body
        service = BankingService(self.request)
        return service.fund_transfer(access_token, fund_json)
