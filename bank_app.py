#!/usr/bin/env python

import uuid
from datetime import date, datetime, time

from pyramid.config import Configurator as PyramidConfigurator
from pyramid.renderers import JSON

import apps
from settings import *
from utils.adapters import uuid_adapter, datetime_adapter
from utils.adapters import time_adapter
from repository.session import set_admin_context


def set_request_id(request):
    return str(uuid.uuid4())


def main(global_config, **settings):
    """
    Entry point for the web application.
    The **settings dict is taken from the /startup .ini files.

    Function returns a Pyramid WSGI application.
    """

    # Create Pyramid application
    app = PyramidConfigurator(settings=settings)

    # Include Pyramid extensions
    app.include('pyramid_beaker')
    app.include('pyramid_rewrite')
    app.include('pyramid_tm')

    # Add a url rewrite rule for any route without a trailing slash in the
    # path_info and append the required trailing slash. This is done by
    # subscribing to the NewRequest event of pyramid to intercept the
    # request and modify the path_info before pyramid performs
    # the route match operation.
    path_match = '(?P<path>(?:/[^_][a-z0-9-._]+)(?:/[a-z0-9-._]+)*)'
    app.add_rewrite_rule(path_match, '%(path)s/')

    json_renderer = JSON()
    json_renderer.add_adapter(uuid.UUID, uuid_adapter)
    json_renderer.add_adapter(date, datetime_adapter)
    json_renderer.add_adapter(datetime, datetime_adapter)
    json_renderer.add_adapter(time, time_adapter)

    app.add_renderer('json', json_renderer)

    app.include('apps.auth', route_prefix='/auth')
    app.include('apps.banking', route_prefix='/banking')

    # Scan application for all views with view_config decorators
    app.scan(apps)

    # Static Views
    app.add_static_view('/', 'frontend', cache_max_age=3600)

    # Register request properties and cache them for repeated access.
    app.add_request_method(set_admin_context, 'admin_context',
                           property=True, reify=True)

    return app.make_wsgi_app()
