import random
from settings import ADMIN_DB_NAME, ADMIN_DB_HOST, ADMIN_DB_HOST_PORT
from settings import ADMIN_DB_USER, ADMIN_DB_PASSWORD, SQL_ENDPOINT_FORMAT

from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema
from db_models.enums import StatusEnum, AccountTypeEnum, GenderEnum, TransactonTypeEnum
from sqlalchemy.orm import sessionmaker

from db_models.models import Address, Account, Branch, Transacton, Beneficiary, Customer
from db_models.auth import AuthToken, AuthUser
from db_models import Base
from sqlalchemy import *
from utils.hashing import PasswordHashHmac


metadata = MetaData()
SAMPLE_USERS = 7
SAMPLE_CUSTOMERS = 6


endpoint = SQL_ENDPOINT_FORMAT.format(ADMIN_DB_USER, ADMIN_DB_PASSWORD,
                                      ADMIN_DB_HOST, ADMIN_DB_HOST_PORT,
                                      ADMIN_DB_NAME)

engine = create_engine(endpoint, echo=True)
engine.execute(CreateSchema('application'))
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


address = Address()
address.city = 'Hyderabad'
address.country = 'India'
address.status = StatusEnum.active.value
address.line_1 = 'Branch Address'
address.state = 'Telangana'
address.zipcode = '000000'
session.add(address)


branch = Branch()
branch.name = 'Bank Branch'
branch.address_id = address.id
branch.status = StatusEnum.active.value
branch.phone = '5645646545'
session.add(branch)

hash_client = PasswordHashHmac()

for i in range(10):

    address = Address()
    address.city = 'Hyderabad'
    address.country = 'India'
    address.status = StatusEnum.active.value
    address.line_1 = 'Abc xyz'
    address.state = 'Telangana'
    address.zipcode = '000000'
    session.add(address)
    session.commit()

    email = 'customer_{}@bank.com'.format(i)
    username = email
    password = '12345678'
    salt = hash_client.generate_salt()
    password = hash_client.hash_password(password, salt)

    user = Customer()
    user.name = 'Customer_{}'.format(i)
    user.address_id = address.id
    user.gender = 'MALE'
    user.age = random.randrange(20, 50)
    user.email = username
    user.phone = '959595959{}'.format(i)
    user.branch_id = branch.id
    session.add(user)
    session.commit()

    auth_user = AuthUser()
    auth_user.customer_id = user.id
    auth_user.password = password
    auth_user.salt = salt
    auth_user.user_id = username
    session.add(auth_user)
    session.commit()

    acc = Account()
    acc.account_type = AccountTypeEnum.savings.value
    acc.balance = 500
    acc.customer_id = user.id
    acc.status = StatusEnum.active.value
    session.add(acc)
    session.commit()

    for j in range(5):
        trans = Transacton()
        trans.account_id = acc.id
        trans.merchant_id = user.id
        trans.merchant_name = user.name
        _type = random.choice(['d', 'w'])
        if _type == 'd':
            trans.transaction_type = TransactonTypeEnum.deposit.value
            trans.amount = random.randint(5000, 10000)
            acc.balance += trans.amount
        else:
            trans.transaction_type = TransactonTypeEnum.withdrawal.value
            amount = random.randint(1000, 5000)
            trans.amount = amount if amount < acc.balance else acc.balance
            acc.balance -= trans.amount
        session.add(acc)
        session.add(trans)
        session.commit()
session.flush()
