from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime

from db_models import EntityBase, ModifiedMixin, CreatedMixin
from db_models.models import Customer


class AuthToken(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a auth tokens for a valid user.
    """
    __tablename__ = 'auth_token'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    customer_id = Column(Integer, ForeignKey(Customer.id,
                                             onupdate='cascade',
                                             ondelete='cascade'))
    access_token = Column(String, nullable=False)
    refresh_token = Column(String, nullable=False)
    expires = Column(DateTime(timezone=True), nullable=False)


class AuthUser(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents the authentication credentials of a user.
    """
    __tablename__ = 'auth_user'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    customer_id = Column(Integer, ForeignKey(Customer.id,
                                             onupdate='cascade',
                                             ondelete='cascade'))
    user_id = Column(String, ForeignKey(Customer.email,
                                        onupdate='cascade',
                                        ondelete='cascade'))
    salt = Column(String, nullable=False)
    password = Column(String, nullable=False)
