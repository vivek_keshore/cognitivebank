from enum import Enum
from sqlalchemy.types import Enum as EnumType


class EnumBase(Enum):
    @classmethod
    def db_type(cls):
        return EnumType(*cls.values(), name=cls.__type_name__,
                        schema=cls.__schema__)

    @classmethod
    def values(cls):
        return [item.value for item in cls]

    @classmethod
    def items(cls):
        return [(item.name, item.value) for item in cls]


class StatusEnum(EnumBase):
    __type_name__ = 'status_enum'
    __schema__ = 'application'
    __order__ = 'active inactive obsolete'

    active = 'ACTIVE'
    inactive = 'INACTIVE'
    obsolete = 'OBSOLETE'


class GenderEnum(EnumBase):
    __type_name__ = 'gender_enum'
    __schema__ = 'application'
    __order__ = 'male female other'

    male = 'MALE'
    female = 'FEMALE'
    other = 'OTHER_GENDER'


class AccountTypeEnum(EnumBase):
    __type_name__ = 'account_type_enum'
    __schema__ = 'application'
    __order__ = 'savings current fixed'

    savings = 'SAVINGS'
    current = 'CURRENT'
    fixed = 'FIXED'


class TransactonTypeEnum(EnumBase):
    __type_name__ = 'transaction_type_enum'
    __schema__ = 'application'
    __order__ = 'withdrawal deposit'

    withdrawal = 'WITHDRAWAL'
    deposit = 'DEPOSIT'
