from datetime import time
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime

from sqlalchemy.orm import relationship
from sqlalchemy.types import Time

from db_models import EntityBase, ModifiedMixin, CreatedMixin
from db_models.enums import StatusEnum, GenderEnum
from db_models.enums import AccountTypeEnum, TransactonTypeEnum
from utils import utc_timestamp


class Address(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a Branch's or Customer's address.
    """
    __tablename__ = 'address'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    line_1 = Column(String, nullable=False)
    line_2 = Column(String)
    landmark = Column(String)
    city = Column(String, nullable=False)
    state = Column(String, nullable=False)
    country = Column(String, nullable=False)
    zipcode = Column(String, nullable=False)
    status = Column(StatusEnum.db_type())


class Branch(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a Branch.
    """
    __tablename__ = 'branch'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    name = Column(String, nullable=False)
    open_time = Column(Time, default=time(10, 0, 0))
    close_time = Column(Time, default=time(17, 0, 0))
    address_id = Column(Integer, ForeignKey(Address.id,
                                            onupdate='cascade',
                                            ondelete='cascade'))
    phone = Column(String, nullable=False)
    status = Column(StatusEnum.db_type())

    # Relational Properties
    address = relationship(
        'Address', primaryjoin='and_(Branch.address_id==Address.id)',
        backref='branch')


class Customer(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a Customer.
    """
    __tablename__ = 'customer'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    name = Column(String, nullable=False)
    age = Column(String, nullable=False)
    gender = Column(GenderEnum.db_type())
    address_id = Column(Integer, ForeignKey(Address.id,
                                            onupdate='cascade',
                                            ondelete='cascade'))
    branch_id = Column(Integer, ForeignKey(Branch.id,
                                           onupdate='cascade',
                                           ondelete='cascade'))
    phone = Column(String, nullable=False, unique=True)
    email = Column(String, nullable=False, unique=True)
    status = Column(StatusEnum.db_type())

    # Relational Properties
    account = relationship('Account', backref='customer')
    beneficiaries = relationship('Beneficiary', primaryjoin='and_(Customer.id==Beneficiary.customer_id)',
                                 backref='customer')
    address = relationship(
        'Address', primaryjoin='and_(Customer.address_id==Address.id)',
        backref='customer')

    branch = relationship(
        'Branch', primaryjoin='and_(Customer.branch_id==Branch.id)',
        backref='customer')


class Account(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents an Account of a customer.
    """
    __tablename__ = 'account'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    customer_id = Column(Integer, ForeignKey(Customer.id,
                                             onupdate='cascade',
                                             ondelete='cascade'))
    status = Column(StatusEnum.db_type())
    account_type = Column(AccountTypeEnum.db_type())
    balance = Column(Integer, default=0, nullable=False)


class Transacton(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a Transactions in a customer's account.
    """
    __tablename__ = 'transaction'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    account_id = Column(Integer, ForeignKey(Account.id,
                                            onupdate='cascade',
                                            ondelete='cascade'))
    merchant_name = Column(String, nullable=False)
    merchant_id = Column(Integer, nullable=False)
    transaction_type = Column(TransactonTypeEnum.db_type())
    timestamp = Column(DateTime(timezone=True), default=utc_timestamp)
    amount = Column(Integer, nullable=False)


class Beneficiary(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a beneficiary of a customer's account.
    """
    __tablename__ = 'beneficiary'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    customer_id = Column(Integer, ForeignKey(Customer.id,
                                             onupdate='cascade',
                                             ondelete='cascade'))
    beneficiary_id = Column(Integer, ForeignKey(Customer.id,
                                                onupdate='cascade',
                                                ondelete='cascade'))
    amount_limit = Column(Integer, nullable=False)
