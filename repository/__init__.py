class RepositoryBase(object):
    def __init__(self, db_context):
        if not db_context:
            raise TypeError('db_context is required.')
        self.db_context = db_context
