from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension
from utils import suuid

from settings import SQL_ENDPOINT_FORMAT
from settings import ADMIN_DB_NAME, ADMIN_DB_HOST, ADMIN_DB_HOST_PORT
from settings import ADMIN_DB_USER, ADMIN_DB_PASSWORD
from collections import namedtuple

MODE_ADMIN = 'admin'
MODE_ADMIN_SCOPED = 'admin-scoped'

factory_set = namedtuple('FactorySet', ['factory', 'registry'])
engine_pool = {}


class SqlContext(object):
    def __init__(self):
        self.session = self.create_session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def create_session(self):
        pass

    def close(self):
        if self.session:
            self.session.close()

    @staticmethod
    def get_endpoint(user, password, host, port, name):
        return SQL_ENDPOINT_FORMAT.format(user, password, host, port, name)

    @staticmethod
    def get_engine(endpoint):
        engine_id = suuid(endpoint)
        engine = engine_pool.get(engine_id)
        if not engine:
            engine = create_engine(endpoint)
            engine_pool[engine_id] = engine
        return engine


class SqlDatabaseContext(SqlContext):
    session_factories = {}

    def __init__(self, mode, factory, user, password, host, port, name):
        if mode not in self.session_factories:
            self.session_factories[mode] = factory

        self.mode = mode
        self.endpoint = self.get_endpoint(user, password, host, port, name)
        self.engine = self.get_engine(self.endpoint)

        super(SqlDatabaseContext, self).__init__()


class AdminDatabaseContext(SqlDatabaseContext):
    def __init__(self, mode=MODE_ADMIN, factory=None, user=ADMIN_DB_USER,
                 password=ADMIN_DB_PASSWORD, host=ADMIN_DB_HOST,
                 port=ADMIN_DB_HOST_PORT, name=ADMIN_DB_NAME):
        if mode not in self.session_factories and not factory:
            session_factory = sessionmaker()
            factory = factory_set(factory=session_factory, registry=None)

        super(AdminDatabaseContext, self).__init__(
            mode, factory, user, password, host, port, name
        )

    def create_session(self):
        factory = self.session_factories[self.mode].factory
        return factory(bind=self.engine)


class AdminDatabaseScopedContext(AdminDatabaseContext):
    def __init__(self, **kwargs):
        factory = None
        if MODE_ADMIN_SCOPED not in self.session_factories:
            extension = ZopeTransactionExtension()
            session_factory = sessionmaker(extension=extension)
            scoped_registry = scoped_session(session_factory)
            factory = factory_set(factory=session_factory,
                                  registry=scoped_registry)

        super(AdminDatabaseScopedContext, self).__init__(
            mode=MODE_ADMIN_SCOPED, factory=factory, **kwargs
        )

    def create_session(self):
        registry = self.session_factories[self.mode].registry
        return registry(bind=self.engine)

    def close(self):
        registry = self.session_factories[self.mode].registry
        registry.remove()
