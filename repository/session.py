from repository.context import AdminDatabaseScopedContext


def set_admin_context(request):
    admin_context = AdminDatabaseScopedContext()
    return admin_context
