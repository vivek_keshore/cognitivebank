from repository import RepositoryBase


class SqlRepositoryBase(RepositoryBase):
    """
    Base Repository for relational database models.
    """
    @property
    def session(self):
        return self.db_context.session
