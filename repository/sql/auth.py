from datetime import timedelta
from repository.sql import SqlRepositoryBase
from db_models.auth import AuthToken, AuthUser
from oauthlib.common import generate_token
from utils import utc_timestamp
from settings import ACCESS_TOKEN_LEN
from settings import TOKEN_EXPIRES_SECS


class AuthUserRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return AuthUser

    def get_all_query(self):
        query = self.session.query(AuthUser)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def get_user_by_username(self, user_id):
        query = self.get_all_query()
        query = query.filter(AuthUser.user_id == user_id)
        return query.scalar()


class AuthTokenRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return AuthToken

    def get_all_query(self):
        query = self.session.query(AuthToken)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def create_token(self, customer_id):
        auth_token = AuthToken()
        auth_token.access_token = str(generate_token(ACCESS_TOKEN_LEN))
        auth_token.refresh_token = str(generate_token(ACCESS_TOKEN_LEN))
        auth_token.customer_id = customer_id
        auth_token.expires = utc_timestamp() + timedelta(seconds=TOKEN_EXPIRES_SECS)

        self.session.add(auth_token)
        self.session.flush()
        return auth_token

    def get_by_token(self, access_token=None, refresh_token=None):
        query = self.get_all_query()
        if access_token:
            query = query.filter(AuthToken.access_token == access_token)
        elif refresh_token:
            query = query.filter(AuthToken.refresh_token == refresh_token)

        return query.scalar()

    def delete_access_token(self, token):
        self.session.delete(token)
        self.session.flush()

        return True