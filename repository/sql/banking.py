from repository.sql import SqlRepositoryBase
from db_models.models import Account, Transacton, Beneficiary


class AccountRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return Account

    def get_all_query(self):
        query = self.session.query(Account)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def get_by_customer_id(self, customer_id):
        query = self.get_all_query()
        query = query.filter(Account.customer_id == customer_id)
        return query.scalar()

    def transfer_funds(self, cust_account, ben_account, amount):
        cust_account.balance -= amount
        ben_account.balance += amount

        self.session.add(cust_account)
        self.session.add(ben_account)
        self.session.flush()

        return True


class TransRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return Transacton

    def get_all_query(self):
        query = self.session.query(Transacton)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def get_all_transactions(self, account_id):
        query = self.get_all_query()
        query = query.filter(Transacton.account_id == account_id)
        return query.all()


class BeneficiaryRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return Beneficiary

    def get_all_query(self):
        query = self.session.query(Beneficiary)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def add_beneficiary(self, cust_id, ben_id, limit):
        beneficiary = Beneficiary()
        beneficiary.beneficiary_id = ben_id
        beneficiary.customer_id = cust_id
        beneficiary.amount_limit = limit

        self.session.add(beneficiary)
        self.session.flush()
        return True

    def get_beneficiary_by_cust_id(self, ben_id, customer_id):
        query = self.get_all_query()
        query = query.filter(Beneficiary.beneficiary_id == ben_id)
        query = query.filter(Beneficiary.customer_id == customer_id)
        return query.scalar()
