from repository.sql import SqlRepositoryBase
from db_models.models import Customer


class CustomerRepository(SqlRepositoryBase):
    @property
    def model_type(self):
        return Customer

    def get_all_query(self):
        query = self.session.query(Customer)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def get_by_id(self, customer_id):
        query = self.get_all_query()
        query = query.filter(Customer.id == customer_id)
        return query.scalar()

    def get_beneficiary(self, email, branch_id):
        query = self.get_all_query()
        query = query.filter(Customer.branch_id == branch_id)
        query = query.filter(Customer.email == email)
        return query.scalar()
