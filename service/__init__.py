class ServiceBase(object):
    def __init__(self, request):
        """
        Base class for all service layer class implementation.
        Mandates the required parameter for the request object from the view.
        :param request: The pyramid.request object available to the view.
        """
        self.request = request

    @staticmethod
    def filter_obj(obj, attributes):
        return {attr: getattr(obj, attr, None) for attr in attributes}
