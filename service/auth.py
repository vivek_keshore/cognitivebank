import httplib
from lepl.apps import rfc3696
from repository.sql.auth import AuthTokenRepository, AuthUserRepository
from repository.sql.customer import CustomerRepository
from service import ServiceBase
from utils import utc_timestamp
from utils.hashing import PasswordHashHmac


email_validator = rfc3696.Email()

class AuthService(ServiceBase):
    def __init__(self, request):
        """
        Initializes required repository and request object.
        """
        super(AuthService, self).__init__(request)
        self.admin_context = request.admin_context
        self.cust_repo = CustomerRepository(request.admin_context)
        self.auth_token_repo = AuthTokenRepository(request.admin_context)
        self.auth_user_repo = AuthUserRepository(request.admin_context)

    def login(self, username, password):
        if not email_validator(username):
            return {httplib.BAD_REQUEST, 'Invalid email address.'}

        auth_user = self.auth_user_repo.get_user_by_username(username)
        if not auth_user:
            return {httplib.NOT_FOUND: 'Invalid user'}

        hash_client = PasswordHashHmac()
        valid = hash_client.check_password(password, auth_user.salt,
                                           auth_user.password)

        if not valid:
            return httplib.FORBIDDEN

        auth_token = self.auth_token_repo.create_token(auth_user.customer_id)
        if auth_token:
            return {
                'customer_id': auth_token.customer_id,
                'access_token': auth_token.access_token,
                'refresh_token': auth_token.refresh_token,
                'expires': auth_token.expires,
                'message': 'Tokens Created Successfully',
                'response_code': httplib.CREATED
            }
        else:
            return {httplib.INTERNAL_SERVER_ERROR: 'Server Error'}

    def logout(self, access_token):
        if not access_token:
            return {httplib.BAD_REQUEST, 'No access token in http header'}

        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}
        status = self.auth_token_repo.delete_access_token(auth_token)
        if status:
            return {httplib.NO_CONTENT: 'Logged out successfully'}
        else:
            return {httplib.INTERNAL_SERVER_ERROR: 'Unable to logout.'}

    def customer_details(self, access_token):
        if not access_token:
            return {httplib.BAD_REQUEST, 'No access token in http header'}

        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}

        customer = self.cust_repo.get_by_id(auth_token.customer_id)
        if not customer:
            return {httplib.NOT_FOUND: 'Customer not found.'}
        account = (customer.account)[0]
        address = customer.address
        response = {
            'customer_id': customer.id,
            'name': customer.name,
            'age': customer.age,
            'gender': customer.gender,
            'address': {
                'line_1': address.line_1,
                'line_2': address.line_2,
                'landmark': address.landmark,
                'city': address.city,
                'state': address.state,
                'country': address.country,
                'zipcode': address.zipcode
            },
            'phone': customer.phone,
            'email': customer.email,
            'status': customer.status,
            'account_id': account.id,
            'branch_id': customer.branch_id,
            'balance': account.balance,
            'account_type': account.account_type
        }
        return response

    def validate_token(self, access_token):
        if not access_token:
            return {httplib.BAD_REQUEST, 'No access token in http header'}

        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return httplib.NO_CONTENT

        if auth_token.expires > utc_timestamp():
            return httplib.OK
        else:
            return httplib.FORBIDDEN