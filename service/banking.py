import httplib
from lepl.apps import rfc3696
from repository.sql.customer import CustomerRepository
from repository.sql.banking import AccountRepository
from repository.sql.banking import TransRepository
from repository.sql.banking import BeneficiaryRepository
from repository.sql.auth import AuthTokenRepository, AuthUserRepository
from service import ServiceBase
from utils.hashing import PasswordHashHmac


email_validator = rfc3696.Email()

class BankingService(ServiceBase):
    def __init__(self, request):
        """
        Initializes required repository and request object.
        """
        super(BankingService, self).__init__(request)
        self.admin_context = request.admin_context
        self.cust_repo = CustomerRepository(request.admin_context)
        self.acc_repo = AccountRepository(request.admin_context)
        self.trans_repo = TransRepository(request.admin_context)
        self.ben_repo = BeneficiaryRepository(request.admin_context)
        self.auth_token_repo = AuthTokenRepository(request.admin_context)
        self.auth_user_repo = AuthUserRepository(request.admin_context)

    def get_balance(self, access_token):
        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}

        account = self.acc_repo.get_by_customer_id(auth_token.customer_id)
        if account:
            return {
                'customer_id': auth_token.customer_id,
                'balance': account.balance,
                'account_type': account.account_type,
                'response_code': httplib.OK
            }
        else:
            return {httplib.INTERNAL_SERVER_ERROR: 'Server Error'}

    def get_summary(self, access_token):
        if not access_token:
            return {httplib.BAD_REQUEST, 'No access token in http header'}

        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}

        account = self.acc_repo.get_by_customer_id(auth_token.customer_id)
        if not account:
            return {httplib.BAD_GATEWAY: 'Invalid Customer Account'}

        transactions = self.trans_repo.get_all_transactions(account.id)
        trans_list = []
        for transaction in transactions:
            trans_data = {
                'merchant_name': transaction.merchant_name,
                'merchant_id': transaction.merchant_id,
                'transaction_type': transaction.transaction_type,
                'timestamp': transaction.timestamp,
                'transaction_amount': transaction.amount
            }
            trans_list.append(trans_data)

        response = {
            'customer_id': account.customer_id,
            'account_id': account.id,
            'balance': account.balance,
            'account_type': account.account_type,
            'transactions': trans_list
        }
        return response

    def add_beneficiary(self, access_token, ben_json):
        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}

        email = ben_json.get('email')
        branch_id = ben_json.get('branch_id')
        amount_limit = ben_json.get('amount_limit')
        account_id = ben_json.get('account_id')
        ben_cust = self.cust_repo.get_beneficiary(email, branch_id)
        if not ben_cust:
            return {httplib.NO_CONTENT: 'Invalid beneficiary details'}

        ben_info = self.ben_repo.get_beneficiary_by_cust_id(
            ben_cust.id, auth_token.customer_id)
        if ben_info:
            return {httplib.BAD_REQUEST: 'Beneficiary already added.'}

        account = (ben_cust.account)[0]
        if account_id != account.id:
            return {httplib.FORBIDDEN: 'Invalid beneficiary details'}

        status = self.ben_repo.add_beneficiary(auth_token.customer_id,
                                               ben_cust.id, amount_limit)

        if status:
            return {httplib.CREATED: 'Beneficiary Added Successfully'}
        else:
            return {httplib.INTERNAL_SERVER_ERROR: 'Error: Beneficiary not added.'}

    def fund_transfer(self, access_token, fund_json):
        auth_token = self.auth_token_repo.get_by_token(access_token)
        if not auth_token:
            return {httplib.NOT_FOUND: 'Invalid Access Token.'}

        ben_id = fund_json.get('beneficiary_id')
        amount = fund_json.get('amount')

        ben_info = self.ben_repo.get_beneficiary_by_cust_id(
            ben_id, auth_token.customer_id)
        if not ben_info:
            return {httplib.NO_CONTENT: 'Invalid beneficiary details'}

        if amount > ben_info.amount_limit:
            return {httplib.NOT_ACCEPTABLE: 'Amount more than beneficiary limit.'}

        customer = self.cust_repo.get_by_id(auth_token.customer_id)
        cust_account = (customer.account)[0]
        if cust_account.balance < amount:
            return {httplib.NOT_ACCEPTABLE: 'Insufficient balance.'}

        beneficiary = self.cust_repo.get_by_id(ben_id)
        ben_account = (beneficiary.account)[0]

        status = self.acc_repo.transfer_funds(
            cust_account, ben_account, amount)

        if status:
            return {httplib.ACCEPTED: 'Fund Transfer Successful'}
        else:
            return {httplib.INTERNAL_SERVER_ERROR: 'Error: Beneficiary not added.'}
