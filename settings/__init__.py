SQL_ENDPOINT_FORMAT = 'postgresql://{0}:{1}@{2}:{3}/{4}'

# ZAdmin DB configurations
ADMIN_DB_HOST = '127.0.0.1'
ADMIN_DB_HOST_PORT = 5432
ADMIN_DB_NAME = 'bank'
ADMIN_DB_USER = 'postgres'
ADMIN_DB_PASSWORD = 'postgres'

TOKEN_EXPIRES_SECS = 900
ACCESS_TOKEN_LEN = 40

HTTP_HEADER_AUTH_TOKEN = 'bank-auth-token'
HTTP_HEADER_REFRESH_TOKEN = 'bank-refresh-token'
