import pytz
import re
import types
from datetime import datetime
from uuid import uuid4, uuid5, NAMESPACE_DNS


def utc_timestamp():
    return pytz.utc.localize(datetime.utcnow())


def strip_spaces_in_string(data):
    if data and isinstance(data, types.StringTypes):
        data = re.sub(r'\s+', ' ', data)
        return data.strip()
    return data


def suuid(value=None, name=None):
    """seeded uuid to generate predictable uuids for testing"""
    if value:
        return value
    else:
        return uuid5(NAMESPACE_DNS, name.lower()) if name else uuid4()
