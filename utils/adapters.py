import pytz
from datetime import date, datetime, time

def uuid_adapter(obj, request=None):
    return str(obj)


def datetime_adapter(obj, request=None):
    if isinstance(obj, (date, datetime)):
        if obj.tzinfo and obj.tzinfo.tzname(obj) == 'UTC':
            tz = pytz.timezone('UTC')
            obj = obj.astimezone(tz)
        return obj.isoformat()
    return str(obj)


def time_adapter(obj, request=None):
    if isinstance(obj, time):
        return obj.isoformat()
