import httplib

from pyramid.authentication import CallbackAuthenticationPolicy
from pyramid.interfaces import IAuthenticationPolicy
from zope.interface import implementer

from settings import HTTP_HEADER_AUTH_TOKEN
from repository.sql.auth import AuthTokenRepository
from repository.sql.customer import CustomerRepository


@implementer(IAuthenticationPolicy)
class TokenAuthenticationPolicy(CallbackAuthenticationPolicy):
    def __init__(self, debug=False):
        self.debug = debug

    def unauthenticated_userid(self, request):
        """ Picks the access_token from the header if present. """
        return (request.headers.get(HTTP_HEADER_AUTH_TOKEN) or
                request.params.get(HTTP_HEADER_AUTH_TOKEN))

    def authenticated_userid(self, request):
        if request.user:
            return request.user.id


def set_request_user(request):
    access_token = request.unauthenticated_userid
    if not access_token:
        return None

    oper_context = request.oper_context
    if not oper_context:
        return None

    auth_repo = AuthTokenRepository(oper_context)
    auth_token = auth_repo.get_by_token(access_token)
    if not auth_token:
        return None

    cust_repo = CustomerRepository(oper_context)

    # Validate if user exists in database
    customer = cust_repo.get_by_id(auth_token.customer_id)

    return customer
