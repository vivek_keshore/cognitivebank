import abc
import hashlib
import hmac

from oauthlib.common import generate_token


class PasswordHashHmac(object):
    MIN_PASSWORD_LENGTH = 8
    DEFAULT_SALT_LENGTH = 40
    INTERNAL_HASH_SCHEME = hashlib.sha1

    def __init__(self):
        self.scheme = 'HMAC'

    def check_password(self, password, salt, hashed_password):
        match_password = self.hash_password(password, salt)
        return hashed_password == match_password

    def hash_password(self, password, salt):
        salt = str(salt)
        if not self.validate_password_complexity(password):
            raise TypeError('password complexity requirements failed.')
        return hmac.new(salt, password, self.INTERNAL_HASH_SCHEME).hexdigest()

    def validate_password_complexity(self, password):
        return password and len(password) >= self.MIN_PASSWORD_LENGTH

    def generate_salt(self, length=DEFAULT_SALT_LENGTH):
        return str(generate_token(length))
